from importlib.metadata import version

from fastapi import FastAPI
from engie_api.router import filter_router

app = FastAPI(title="Engie Tech Test", docs_url="/", version=version("engie_api"))


def app_factory() -> FastAPI:
    """
    build and return a simple app with router including
    :return: WSGI compatible application
    """
    app.include_router(filter_router)
    return app
