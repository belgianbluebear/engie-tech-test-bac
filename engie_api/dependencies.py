"""
Processor Dependency, exposing its children dependencies to the query string from the endpoints
"""

from engie_api.processors import filters, operations
from engie_api.processors.enums import FiltersEnum, OperationsEnum


class Processor:
    """
    Class based dependency for modular composition from endpoint's query string
    """
    def __init__(
            self, filter_function: FiltersEnum = FiltersEnum.NUMBERS,
            operation: OperationsEnum = OperationsEnum.MULTIPLE_OF,
            operand: int = 3,
            sort: bool = False,
    ):
        self._filter_function = getattr(filters, f"filter_{filter_function}")
        self._operation = getattr(operations, f"op_{operation}")
        self._operand = operand
        self._sort = sort

    def __call__(
            self,
            payload: list = None
    ):
        """
        execute logic with parameters from endpoint's query string
        :param payload:
        :return: Any execution result
        """
        return self._filter_function(
            payload=payload,
            operation=(self._operation, self._operand,),
            sort=self._sort,
        )
