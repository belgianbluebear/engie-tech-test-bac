"""
base router for serving filter endpoint
basic, letting fastapi manage openapi schema, and responses
"""

from fastapi import APIRouter, Depends, Body
from engie_api.dependencies import Processor

filter_router = APIRouter(prefix="/filter")


@filter_router.post("/")
async def filter_numbers(
        payload: list = Body(example=[6, 3.0, "3+0j", 4.1, 3]),
        processor: Processor = Depends(Processor)
):
    return processor(payload)
