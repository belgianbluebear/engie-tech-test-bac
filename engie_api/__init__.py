"""
Engie Tech test on simple numeric processor

Scope of this tech test:

Create a REST API with one endpoint /filter expecting a JSON body containing a list of numbers (any type of numbers).
Return a JSON response containing a list based on the provided input list
(consider that this list could contain thousands of records).
The JSON response can only contain the values of the input list which are a multiple of 3.
Additionally the response list should be sorted.
The project does not need to contain the whole setup to make it deployable.
However, in addition to the code of the application,
it should contain a test suite ensuring that those specifications are matched.

Below is a sample API request with the expected response (HTTP 200):

Example 1 request:
 - POST /filter [17, 15, 7, 5, 29, 36 , 4 , 1, 3]
 - Example 1 response body: [36, 15, 3]

Example 2 request:
 - POST /filter [17.2, 15.0, 7.8, 5.3, 27.1, 36.0 , 4.1 , 1.0, 3.0]

 - Example 2 response body: [36.0, 15.0, 3.0]
"""