"""
starts a uvicorn server for debugging purposes (and exposes the openapi swagger)
"""
from uvicorn import run
from engie_api.app import app_factory


def run_api():
    run(app_factory, factory=True)
