"""
Filters are the first level of processing
a Filter must be written as:
name must begin with 'filter_'
signature must respect the format Iterable + Tuple of options
"""

from typing import Any
from fastapi.exceptions import HTTPException


def filter_numbers(
        payload: list[str | float | int],
        operation: tuple[callable, Any] = (None, 3,),
        sort: bool = False) -> list[str | float | int]:
    """
    Parse, extract and optionally sort any type of number
    :param operation: how to decide if we keep the number
    :param payload: list of number to filter
    :param sort: returned sorted or not
    :return: filtered and/or sorted numbers
    """
    filter_function, operand = operation
    try:
        result = [i for i in filter_function(payload, operand)]
        if sort:
            result = sorted(result, key=lambda x: abs(complex(x)) if isinstance(x, str) else x, reverse=True)
        return result
    except ValueError as e:
        raise HTTPException(detail=str(e), status_code=422)
