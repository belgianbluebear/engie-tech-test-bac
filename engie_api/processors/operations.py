"""
Operations is the second level of processing
Injected into Filters, they can get any list of items,
and get its own custom kwargs from the options Tuple from processor
Can't raise any HTTP level Exception, as it's delegated to the Filter layer
"""


def op_multiple_of(payload: list, operand: int):
    """
    Multiple of processor generator
    :param payload: list of numbers to process
    :param operand: int for comparison
    :return: numbers as generator
    """
    if operand == 0:
        raise ValueError("Operand can't be 0.")
    for number in payload:
        if isinstance(number, str):
            try:
                if abs(complex(number)) % operand in [0, 0.0]:
                    yield number
            except (TypeError, ValueError) as e:
                raise ValueError() from e
        elif isinstance(number, (float, int)):
            if number % operand in [0, 0.0]:
                yield number
        else:
            raise ValueError(f"{number} is not a supported numeric type.")
