"""
Dynamical enums for exposing all filters and operations without writing schema declaration
"""

from enum import Enum
from engie_api.processors import operations, filters


OperationsEnum = Enum(
    'OperationsEnum',
    {
        func.upper().replace("OP_", ""): func.casefold().replace("op_", "")
        for func in operations.__dict__.keys() if func.startswith("op_")
    }
    ,
    type=str
)


FiltersEnum = Enum(
    'FiltersEnum',
    {
        func.upper().replace("FILTER_", ""): func.casefold().replace("filter_", "")
        for func in filters.__dict__.keys() if func.startswith("filter_")
    }
    ,
    type=str
)
