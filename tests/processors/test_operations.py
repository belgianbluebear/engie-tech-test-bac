import pytest
from engie_api.processors.operations import op_multiple_of


@pytest.mark.parametrize(
    "payload,operand,expected",
    [
        (
            [2, 4.0, "6+0j", 1],
            2,
            [2, 4.0, '6+0j'],
        ),
        (
            [0, 8,  4.0, "6+0j", 1],
            2,
            [0, 8, 4.0, '6+0j'],
        ),
    ]
)
def test_multiple_of(payload, operand, expected):
    assert [i for i in op_multiple_of(payload, operand)] == expected
