import pytest

from engie_api.processors.filters import filter_numbers
from engie_api.processors.operations import op_multiple_of

from fastapi.exceptions import HTTPException


@pytest.mark.parametrize(
    "payload,operation,operand,sort,expected",
    [
        (
            ["6+0j", 1, 2, 3, 4.0, 5],
            op_multiple_of,
            2,
            True,
            ["6+0j", 4.0, 2]
        ),
        (
            ["6+0j", 1, 2, 3, 4.0, 5],
            op_multiple_of,
            2,
            False,
            ["6+0j", 2, 4.0]
        ),
    ]
)
def test_filter_numbers_ok(payload, operation, operand, sort, expected):
    assert filter_numbers(payload, (operation, operand,), sort) == expected


@pytest.mark.parametrize(
    "payload,operation,operand,exception",
    [
        (
            [],
            op_multiple_of,
            0,
            HTTPException
        ),
        (
            ["6+0x"],
            op_multiple_of,
            2,
            HTTPException
        ),
    ]
)
def test_filter_numbers_raises(payload, operation, operand, exception):
    with pytest.raises(exception):
        filter_numbers(payload, (operation, operand,))
