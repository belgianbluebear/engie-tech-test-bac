import pytest
from fastapi.testclient import TestClient
from engie_api.app import app_factory


@pytest.fixture(scope="session")
def app():
    yield app_factory()


@pytest.fixture(scope="session")
def test_client(app):
    yield TestClient(app)
