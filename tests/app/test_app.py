from fastapi import FastAPI


def test_app_factory(app):
    assert app and isinstance(app, FastAPI)


def test_router(app):
    assert any([route.name == "filter_numbers" for route in app.routes])
