import pytest
from payloads import filter_numbers_cases_ok, filter_numbers_raises


@pytest.mark.parametrize(
    "filter_func,operation,operand,sort,test_input,exception",
    filter_numbers_raises
)
def test_number_filter_raises(test_client, filter_func, operation, operand, sort, test_input, exception):
    with pytest.raises(exception):
        response = test_client.post(
            f"/filter/",
            params={
                "filter_function": filter_func,
                "operation": operation,
                "operand": operand,
                "sort": sort,
            },
            json=test_input,
        )
        assert response.status_code == 422


@pytest.mark.parametrize(
    "filter_func,operation,operand,sort,test_input,expected",
    filter_numbers_cases_ok
)
def test_number_filter_ok(test_client, filter_func, operation, operand, sort, test_input, expected):
    response = test_client.post(
        f"/filter/",
        params={
            "filter_function": filter_func,
            "operation": operation,
            "operand": operand,
            "sort": sort,
        },
        json=test_input,
    )
    assert response.ok
    assert response.json() == expected
