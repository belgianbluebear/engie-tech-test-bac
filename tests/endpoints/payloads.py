from engie_api.processors.enums import FiltersEnum, OperationsEnum
from fastapi.exceptions import HTTPException

filter_numbers_cases_ok = [
    (
        FiltersEnum.NUMBERS,
        OperationsEnum.MULTIPLE_OF,
        3,
        True,
        [17, 15, 7, 5, 29, 36, 4, 1, 3],
        [36, 15, 3],
    ),
    (
        FiltersEnum.NUMBERS,
        OperationsEnum.MULTIPLE_OF,
        3,
        True,
        [17.2, 15.0, 7.8, 5.3, 27.1, 36.0, 4.1, 1.0, 3.0],
        [36.0, 15.0, 3.0],
    ),
    (
        FiltersEnum.NUMBERS,
        OperationsEnum.MULTIPLE_OF,
        3,
        False,
        [1, 2, 3, 4, 5, 6.0, 7, "+1j", "9+0j"],
        [3, 6.0, "9+0j"],
    ),
    (
        FiltersEnum.NUMBERS,
        OperationsEnum.MULTIPLE_OF,
        -3,
        False,
        [1, 2, 3, 4, 5, 6.0, 7, "+1j", "9+0j"],
        [3, 6.0, "9+0j"],
    ),
    (
        FiltersEnum.NUMBERS,
        OperationsEnum.MULTIPLE_OF,
        3,
        True,
        ["9+0j", "8+1j", 7, 6.0, 5, 4, 3, 2, 1],
        ["9+0j", 6.0, 3],
    ),
]

filter_numbers_raises = [
    (
        FiltersEnum.NUMBERS,
        OperationsEnum.MULTIPLE_OF,
        2,
        False,
        [b"", "string"],
        TypeError,
    ),
]
